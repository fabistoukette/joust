var GameOver = function () { };

GameOver.prototype = {

    preload: function () {
        this.optionCount = 1;
    },

    addMenuOption: function (text, callback) {
        var optionStyle = { fill: 'white', align: 'left', stroke: 'rgba(0,0,0,0)', srokeThickness: 4 };
        var txt = game.add.text(game.world.centerX, (this.optionCount * 80) + 150, text, optionStyle);
        txt.anchor.setTo(0.5);
        txt.stroke = "rgba(0,0,0,0";
        txt.strokeThickness = 4;
        var onOver = function (target) {
            target.fill = "#FEFFD5";
            target.stroke = "rgba(200,200,200,0.5)";
            txt.useHandCursor = true;
        };
        var onOut = function (target) {
            target.fill = "white";
            target.stroke = "rgba(0,0,0,0)";
            txt.useHandCursor = false;
        };
        //txt.useHandCursor = true;
        txt.inputEnabled = true;
        txt.events.onInputUp.add(callback, this);
        txt.events.onInputOver.add(onOver, this);
        txt.events.onInputOut.add(onOut, this);

        this.optionCount++;
    },

    create: function () {
        // game.add.sprite(0, 0, 'gameover-bg');
        var winner= "";
        if (NoMoreTime) {winner= "NO MORE TIME!";}
        else if(WinJ1) {winner= "YOU WON!";}
        else if(WinCPU) {winner= "YOU LOST!";}
        else {winner= "GAME OVER!";}
        var titleStyle = { fill: '#FDFFB5', align: 'center' };
        var text = game.add.text(game.world.centerX, 100, winner, titleStyle);
        text.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        text.anchor.set(0.5);
        this.addMenuOption('Play Again', function (e) {
            this.game.state.start("game");
        });
        this.addMenuOption('Main Menu', function (e) {
            this.game.state.start("menu");
        })


    }
};