var Menu = function () { };

Menu.prototype = {

    init: function () {
        this.titleText = game.make.text(game.world.centerX, 100, "JOUST", {
            // font: 'bold 60pt TheMinion',
            fill: '#FDFFB5',
            align: 'center'
        });
        this.titleText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        this.titleText.anchor.set(0.5);
        this.optionCount = 1;
    },

    create: function () {
        // game.add.sprite(0, 0, 'stars');
        game.add.existing(this.titleText);
        game.stage.disableVisibilityChange = true;

        this.addMenuOption('Start', function (target) {
            game.state.start("game");
        });
        this.addMenuOption('Options', function (target) {
            game.state.start("options");
        });
        this.addMenuOption('Credits', function (target) {
            game.state.start("credits");
        });
    },

    addMenuOption: function (text, callback) {
        var optionStyle = { fill: 'white', align: 'left', stroke: 'rgba(0,0,0,0)', srokeThickness: 4 };
        var txt = game.add.text(game.world.centerX, (this.optionCount * 80) + 150, text, optionStyle);
        txt.anchor.setTo(0.5);
        txt.stroke = "rgba(0,0,0,0";
        txt.strokeThickness = 4;
        var onOver = function (target) {
            target.fill = "#FEFFD5";
            target.stroke = "rgba(200,200,200,0.5)";
            txt.useHandCursor = true;
        };
        var onOut = function (target) {
            target.fill = "white";
            target.stroke = "rgba(0,0,0,0)";
            txt.useHandCursor = false;
        };
        //txt.useHandCursor = true;
        txt.inputEnabled = true;
        txt.events.onInputUp.add(callback, this);
        txt.events.onInputOver.add(onOver, this);
        txt.events.onInputOut.add(onOut, this);

        this.optionCount++;
    },
};

