var Load = function () { };

Load.prototype = {

    init: function () {
        this.loadingBar = game.make.sprite(game.world.centerX - (387 / 2), 400, "loading");
        this.status = game.make.text(game.world.centerX, 380, 'Loading...', { fill: 'white' });
        utils.centerGameObjects([this.status]);
    },

    // The preload function then will call all of the previously defined functions:
    preload: function () {
        game.add.sprite(0, 0, 'stars');
        game.add.existing(this.status);
        game.add.existing(this.loadingBar);
        this.load.setPreloadSprite(this.loadingBar);

        this.loadScripts();
        this.loadImages();
        this.loadFonts();
        this.loadBgm();
    },

    loadScripts: function () {
        game.load.script('menu', 'menu.js');
        game.load.script('game', 'game.js');
        game.load.script('gameOver', 'gameOver.js');
        game.load.script('credits', 'credits.js');
        game.load.script('options', 'options.js');
    },

    loadImages: function () {
        // game.load.image('menu-bg', 'assets/images/menu-bg.jpg');
        // game.load.image('options-bg', 'assets/images/options-bg.jpg');
        // game.load.image('gameover-bg', 'assets/images/gameover-bg.jpg');
        game.load.image('sky', 'assets/images/sky.png');
        game.load.image('ground', 'assets/images/platform.png');
        //Sprites
        game.load.spritesheet('dude', 'assets/images/dude.png', 32, 48);
        game.load.spritesheet('enemy', 'assets/images/enemy.png', 32, 48);
    },

    loadFonts: function () {
    },

    loadBgm: function () {
        //Audio
        game.load.audio('boden', ['assets/sounds/bodenstaendig_2000_in_rock_4bit.mp3', 'assets/sounds/bodenstaendig_2000_in_rock_4bit.ogg']);
        game.load.audio('hit', 'assets/sounds/ballHit.ogg');
        game.load.audio('bounce', 'assets/sounds/ballBounce.ogg');
        game.load.audio('miss', 'assets/sounds/ballMissed.ogg');
    },


    addGameStates: function () {
        game.state.add("menu", Menu);
        game.state.add("game", Game);
        game.state.add("gameOver", GameOver);
        game.state.add("credits", Credits);
        game.state.add("options", Options);
    },

    addGameMusic: function () {
        musicPlayer = game.add.audio('boden');
        musicPlayer.loop = true;
        // musicPlayer.play();
    },

    create: function () {
        this.status.setText('Ready!');
        this.addGameStates();
        this.addGameMusic();

        setTimeout(function () {
            game.state.start("menu");
        }, 1000);
    }
}
