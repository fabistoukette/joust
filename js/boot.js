var
    game = new Phaser.Game(800, 600, Phaser.AUTO, 'game'),
    Boot = function () { },
    gameOptions = {
        //Audio
        playSound: true,
        playMusic: true,
        //Board
        screenWidth: 800,
        screenHeight: 600,
        //Physics
        playerVelocity: 150,
        playerGravity: 300,
        //
        gameTimer:30
    },
    musicPlayer,
    NoMoreTime=false,
    WinJ1=false,
    WinCPU=true;

Boot.prototype = {
    preload: function () {
        game.load.image('stars', 'assets/images/stars.jpg');
        game.load.image('loading', 'assets/images/loading.png');
        // game.load.image('brand', 'assets/images/logo.png');
        game.load.script('utils', 'utils.js');
        game.load.script('load', 'load.js');
    },

    create: function () {
        game.state.add('load', Load);
        game.state.start('load');
    }
};

game.state.add('boot', Boot);
game.state.start('boot');