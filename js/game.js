var Game = function () { };

Game.prototype = {

    preload: function () {

    },

    create: function () {
        this.initGraphics();
        this.initPhysics();
        this.initSounds();
        cursors = game.input.keyboard.createCursorKeys();
        // input
        pauseKey = this.input.keyboard.addKey(Phaser.Keyboard.P);
        game.input.onDown.add(this.unpause, self);
        timer = game.time.create(false);
        timer.loop(Phaser.Timer.SECOND * gameOptions.gameTimer, this.gameOver, this);
        timer.start();
        NoMoreTime = false;
        WinJ1 = false;
        WinCPU = true;
    },

    gameOver: function(){
        timer.stop(false);
        if (!timer.running)
            NoMoreTime = true;

        game.state.start("gameOver");
    },

    initGraphics: function () {
        //map
        // game.add.sprite(0, 0, 'sky');
        platforms = game.add.group();
        platforms.enableBody = true;
        ground = platforms.create(0, game.world.height - 64, 'ground');
        ground.scale.setTo(2, 2);
        ground.body.immovable = true;
        ledge = platforms.create(400, 400, 'ground');
        ledge.body.immovable = true;
        ledge = platforms.create(-150, 250, 'ground');
        ledge.body.immovable = true;
        //player1
        player = game.add.sprite(32, game.world.height - 150, 'dude');
        player.animations.add('left', [0, 1, 2, 3], 10, true);
        player.animations.add('right', [5, 6, 7, 8], 10, true);
        //enemy
        enemy = game.add.sprite(736, game.world.height - 150, 'enemy');
        enemy.animations.add('left', [0, 1, 2, 3], 10, true);
        enemy.animations.add('right', [5, 6, 7, 8], 10, true);
        //instructions
        game.add.text(16, 16, "Il faut sauter sur la tête de l'adversaire avant qu'il vous saute dessus... VOUS AVEZ 30 SECONDES!!", { font: '12px Arial', fill: '#ff0' });
        game.add.text(16, 32, "Vous pouvez changer de direction uniquement lorsque vous êtes au sol ou que vous rencontrez un obstacle.", { font: '12px Arial', fill: '#ff0' });
        game.add.text(16, 48, "Vous pouvez sauter autant que vous le souhaitez (mais vous allez disparaitre de l'écran).", { font: '12px Arial', fill: '#ff0' });
        game.add.text(16, 64, "....Et oui, la plateforme la plus haute bug et vous pouvez passez au travers dans un certain cas. Mais, c'est une feature!", { font: '12px Arial', fill: '#ff0' });
        timerText = game.add.text(gameOptions.screenWidth * 0.5, gameOptions.screenHeight * 0.30, "0", { font: '32px Arial', fill: '#ff0' });
        timerText.anchor.set(0.5, 0);
    },

    initPhysics: function () {
        //player
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.physics.arcade.enable(player);
        player.body.bounce.y = 0.2;
        player.body.gravity.y = gameOptions.playerGravity;
        // player.body.collideWorldBounds = true;
        player.body.velocity.x = gameOptions.playerVelocity;
        //enemy        
        game.physics.arcade.enable(enemy);
        enemy.body.bounce.y = 0.2;
        enemy.body.gravity.y = 300;
        // enemy.body.collideWorldBounds = true;
        enemy.body.velocity.x = -150;
    },

    initSounds: function () {
        // sounds
    },

    update: function () {
        game.physics.arcade.collide(player, platforms);
        game.physics.arcade.collide(enemy, platforms);
        game.physics.arcade.collide(player, enemy, this.collideSprite);
        if (game.input.keyboard.isDown(Phaser.Keyboard.P)) {
            this.setPause();
        }
        if (player.body.x < -8) { player.body.x = gameOptions.screenWidth }
        if (player.body.x > gameOptions.screenWidth) { player.body.x = 0 }
        if (enemy.body.x < -8) { enemy.body.x = gameOptions.screenWidth }
        if (enemy.body.x > gameOptions.screenWidth) { enemy.body.x = 0 }

        if (cursors.up.isDown) {
            player.body.velocity.y = -120;
        }
        if (cursors.left.isDown && player.body.touching.down) {
            player.body.velocity.x = -gameOptions.playerVelocity;
        }
        if (cursors.right.isDown && player.body.touching.down) {
            player.body.velocity.x = gameOptions.playerVelocity;
        }
        if (player.body.touching.right) {
            player.body.velocity.x = -gameOptions.playerVelocity;
        }
        if (player.body.touching.left) {
            player.body.velocity.x = gameOptions.playerVelocity;
        }
        if (enemy.body.touching.right) {
            enemy.body.velocity.x = -150;
        }
        if (enemy.body.touching.left) {
            enemy.body.velocity.x = 150;
        }
        if (player.body.velocity.x > 0) { player.animations.play('right') }
        if (player.body.velocity.x < 0) { player.animations.play('left') }
        if (enemy.body.velocity.x > 0) { enemy.animations.play('right') }
        if (enemy.body.velocity.x < 0) { enemy.animations.play('left') }

        this.killPlayer();

        timerText.text ='';
        if (timer.running) {
        //     //timerText.alpha = 1;
        //     // if (timer.duration / 1000 < 0.5) {
        //     //     timerText.text = 'Loser!';
        //     // } else {
                timerText.text = Math.round(timer.duration / 1000);
        //    //}
        }
    },

    render: function () {
        // game.debug.body(player);
        // game.debug.body(enemy);
        // game.debug.text("player position: " + player.body.x + ' -' + player.body.y, game.world.centerX - 110, 16);
        // game.debug.text("enemy position: " + enemy.body.x + ' -' + enemy.body.y, game.world.centerX - 110, 32);
    },

    collideSprite: function () {
        if (enemy.body.touching.up && player.body.touching.down) {
            enemy.kill();
            WinJ1 = true;
            WinCPU = false;
            game.state.start("gameOver");
        } else if (enemy.body.touching.down && player.body.touching.up) {
            player.kill();
            WinJ1 = false;
            WinCPU = true;
            game.state.start("gameOver");
        }
    },

    killPlayer: function () {
        if (enemy.body.velocity.x == -player.body.velocity.x) {
            if ((enemy.body.x - player.body.x < 190) && (enemy.body.x - player.body.x > 97)) {
                console.log('saute!')
                enemy.body.velocity.y = -120;
            }
        }
        // if ((player.body.x < (enemy.body.x - 150)) && (player.body.y == enemy.body.y)) {
        //     enemy.body.velocity.y = -120;
        // }
    },

    setPause: function () {
        game.paused = true;
        console.log('pause');
    },

    unpause: function (event) {
        if (game.paused) {
            console.log('unpause');
            game.paused = false;
        }
    },
}    